import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors: any = ['Lewis Carrol', 'Leo Tolstoy', 'Thomas Mann'];
  newauthor: any;
  i:number =3;

  getAuthors(){ 
    const authorsObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
  }
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges();
  }

  //addAuthors = (name) => this.authors.push(name);
  addAuthors(newAuthor:string){
    this.i=this.i+1;
    this.authors.push({id:this.i, name:newAuthor}); 
  }


 
 
  constructor(private db:AngularFirestore) { }
}
