import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  postsData$: Observable<any[]>;
  userId:string
  //users$: Observable<any[]>;

  constructor(private postsService:PostsService,
              public authservice:AuthService) { }
  /*
  user$: Users[]=[];
  body:string;
  title:string;
  author:string;
  pushAlert:string;

  
  pushPosts(){
    for(let i=0;i<this.postsData$.length;i++){
      for(let j=0;j<this.user$.length;j++){
        if(this.postsData$[i].userId==this.user$[j].id){
          this.body = this.postsData$[i].body;
          this.title = this.postsData$[i].title;
          this.author = this.user$[j].name;
          this.postsService.addPosts(this.body,this.title,this.author);
        }
      }
    }
    this.pushAlert = "Posts have been saved to FireStore"
  }
*/
  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.postsData$ = this.postsService.getPosts(this.userId);
      }
    )
 
    /*this.postsData$ = this.postsService.getPosts();
    
    this.postsService.getPosts().subscribe(data => this.postsData$ = data);
    this.postsService.getUsers().subscribe(data => this.user$ = data);
    
    this.user$ = this.postsService.getUsers();

    this.postsData$.subscribe(
      data=>{
        this.posts=data;
        console.log("Huston we have a problem");
      })
    */
  }
  deletePost(id:string){
    this.postsService.deletePost(id, this.userId);
  }



}
