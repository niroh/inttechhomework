import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  private PostsURL = "https://jsonplaceholder.typicode.com/posts/";
  private UsersURL = "https://jsonplaceholder.typicode.com/users/";

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }

  getPosts(userId:string): Observable<any[]>{
    //return this.db.collection('posts').valueChanges(({idField:'id'}));
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
  
      )
    )
  }
  
     
                 
  /*getUsers(){
    return this.http.get<Users[]>(this.UsersURL);
      }*/

  addPost(userId:string, title:string, body:string, author:string){
     const post = {title:title, body:body, author:author}
     //this.db.collection('posts').add(post);
     this.userCollection.doc(userId).collection('posts').add(post);
  }

  updatePost(userId:string, id:string, title:string, body:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      title:title,
      body:body,
      author:author
    })
  }
  getPost(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()  }
  


  deletePost(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete(); 
  }


}
