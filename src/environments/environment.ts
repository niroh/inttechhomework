// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAkoBM0nZePQGIrvZK_oltsoLJ2zoT4B64",
    authDomain: "inttech2019hw.firebaseapp.com",
    databaseURL: "https://inttech2019hw.firebaseio.com",
    projectId: "inttech2019hw",
    storageBucket: "inttech2019hw.appspot.com",
    messagingSenderId: "698409235574",
    appId: "1:698409235574:web:657ad5911f073bcbfedb57"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
